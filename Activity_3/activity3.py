#ACTIVITY 3

#Number 1
print("1.")

from datetime import date #imports the date class from the datetime module. The date class allows us to work with dates in Python.

startdate = date(2014, 7, 2)
enddate = date(2014, 7, 11)
noofdays = enddate - startdate #Subtract to get the date
print(str(noofdays.days) + " days")
print("______________________________________\n\n")
#Number 1 END


#Number 2
print("2.")

int_number = int(input("Enter a number: "))

if int_number % 2 == 0: #Modulo is used to detect for the remainder    
    print(f"{int_number} is even.") #The f inside the print is called Formatted string  # Used to execute the expresion encapsulated by bracket
else:
    print(f"{int_number} is odd.")
print("______________________________________\n\n")
#Number 2 END


#Number 3
print("3.")

def number_range(num): #Declared Function
    return abs(1000 - num) <= 100 or abs(2000 - num) <= 100 #Abs function is used, function that returns the absolute value of a number.

numbers = [1000, 900, 800, 2200]
for num in numbers:
    print(f"{num} = {number_range(num)}")

print("______________________________________\n\n")
#Number 3 END

#Number 4
print("4.")

#Variable Declared
amount = 10000
interest_rate = 3.5
years = 7

#Interest rate need to divided by 100 to get the percentage
future_value = amount * (1 + (interest_rate / 100)) ** years # The ** code is used to multiply itself to that number
print(f"Output2: {future_value:.2f}")

print("______________________________________\n\n")
#Number 4 END

#Number 5
print("5.")
# Method 1
STR_Restart = "restart"
STR_Replace = STR_Restart.replace('r', '$') # all R is replaced by $ $esta$t
STR_Revert = "r" + STR_Replace[1:] # The first R is reverted  resta$t
print(STR_Revert)

# Method 2
STR_Restart2 = 'restart'
STR_Store_FirstLetter = STR_Restart2[0]  # store the first character 'restart' STR_Store_FirstLetter = r
STR_Modified = STR_Store_FirstLetter + STR_Restart2[1:].replace(STR_Store_FirstLetter, '$')  #the function [1:] used to jump to the 2nd character and execute the replace on that block 
print(STR_Modified)

print("______________________________________\n\n")
#Number 5 END


#Number 6
print("6.")

from datetime import datetime #to use the datetimelibrary
from dateutil.relativedelta import relativedelta # pip install python-dateutil , this package must be installed in order to use the relativedelta library

date1 = datetime(2023, 6, 15)
date2 = datetime(2023, 1, 1)

def process_values(val1, val2): #Declared Function that handles 2 parameter
    if type(val1) == type(val2):  #Checking if the 2 variable has the same data types
        if isinstance(val1, str): #Checking if the  val1 is string
            return val1 + val2
        elif isinstance(val1, int): #Checking if the  val1 is integer
            return val1 * val2
        elif isinstance(val1, list): #Checking if the  val1 is list
            return val1 + val2
        elif isinstance(val1, dict): #Checking if the  val1 is dictionary
            return {**val1, **val2}
        elif isinstance(val1, datetime):
            delta = relativedelta(months=val2.month, days=val2.day)
            return val1 + delta
    else:
        return "Values are not the same type."

# Example usage
print(process_values("Alvan", " David"))
print(process_values(5, 3))
print(process_values([1, 2, 3], [4, 5, 6]))
print(process_values({"a": 1}, {"b": 2}))
print(process_values(date1, date2))

print("______________________________________\n\n")
#Number 6 END


#Number 7
print("7.")

class Pet:  #New Class Pet Declared
    def __init__(self, dog_name, dog_breed, dog_color):  #__init__ used when object class if created, paramaters: "self" refers to the instance object being created; dog_name, dog_breed and dog_color are the parameters
        self.dog_name = dog_name  # initializing the instance attribute
        self.dog_breed = dog_breed
        self.dog_color = dog_color
    
    def get_pet_info(self):
        return f"I bought a {self.dog_color} {self.dog_breed} and I named my dog {self.dog_name}"


my_pet = Pet("Lily", "Shih", "White") #my_pet represents the object of the Class Pet
print(my_pet.get_pet_info()) # Used to execute the get_pet_info function

print("______________________________________\n\n")
#Number 7 END


#Number 8
print("8.")

class UppercasePet(Pet):  #New Class UppercasePet Declared, Class pet inheritted the class attribute.
    def get_pet_nametoupper(self):
        return f"{self.dog_name.upper()}"  # upper() function is used to make the string into UpperCase

uppercase_pet = UppercasePet("Lily", "Shih", "White") #uppercase_pet represents the object of the Class Pet
print(uppercase_pet.get_pet_nametoupper())  # Used to execute the get_pet_nametoupper() function

print("______________________________________\n\n")
#Number 8 END

#Number 9
print("9.")

class Person: #Parent Class 
    def __init__(self, name): 
        self.name = name  #Contructor 
    def greet(self):
        print(f"Hello, my name is {self.name}") #Contructor 

class Employee(Person):   #Child Class 
    def __init__(self, name, employee_id):
        super().__init__(name)  # Only child class can used it, to access every function and attribute in parent class. required to add every parameter of the parent
        self.employee_id = employee_id #Contructor 
    def greet(self):  #Child Class 
        super().greet()  # Calling the parent class's function, the child can add another contructor without changing the parent
        print(f"I am an employee with ID {self.employee_id}") #Contructor another contructor

employee = Employee("John", 12345)
employee.greet()


print("______________________________________\n\n")
#Number 9 END

