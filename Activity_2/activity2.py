# ACTIVITY 2
# Number 1 
print("1.")
# \n to nextline the following sentence, \t to tab the sentence
print("Twinkle, twinkle, little star\n How I wonder what you are!\n\tUp above the world so high,\n\tLike a diamond in the sky.\nTwinkle, twinkle, little star,\n How I wonder what you are")

print("______________________________________\n\n")
# Number 1 END

# Number 2
print("2.")
import sys # To use the sys library
print(sys.version)

print("______________________________________\n\n")
# Number 2 END

# Number 3
print("3.")
EnterString = input("Enter your String (special character included): ")
print("The total number of string in " + EnterString + " is " + str(len(EnterString)) + " Characters") # len() function is used to get the length of specified variable.

print("______________________________________\n\n")
# Number 3 END

# Number 4
print("4.")
myname = input("Enter your Name: ")
myage = input("Enter your Age: ")
print("Hello, my name is " + str(myname) + " and my age is " + myage)

print("______________________________________\n\n")
# Number 4 END

# Number 5
print("5.")
import datetime # To use the datetime library

print(datetime.datetime.now())

print("______________________________________\n\n")
# Number 5 END

# Number 6
print("6.")

input_seq_numbers = input("Enter a sequence of comma-separated numbers: ")
number_list = [int(num) for num in input_seq_numbers.split(",")]
number_tuple = tuple(str(num) for num in number_list)

print("\nTuple:", number_tuple) 

print("______________________________________\n\n")
# Number 6 END


# Number 7
print("7.")
color_list = ["Red","Green","White" ,"Black", "Pink"]
print("The Color list: " + color_list[0] + " " + color_list[-1])  # color_list[0] to return the 1st Item,  color_list[-1] to return the last item, -1 to avoid the outofbounds error
print("______________________________________\n\n")
# Number 7 END


# Number 8
print("8.")
elements = ['Hello', ' ', 'alvan', '!']
concatenated_string = ''.join(str(element) for element in elements) #using the join method to concatenate the elements into a single string
print(concatenated_string)

print("______________________________________\n\n")
# Number 8 END

# Number 9
print("9.")
STR_InputNum = input("Sample value of n is: ") # STR_InputNumber is string: 5
STR_InputNum_Twice = STR_InputNum + STR_InputNum   # STR_InputNumber_Twice store the STR_InputNumber twice: 55
STR_InputNum_Thrice = STR_InputNum_Twice + STR_InputNum # STR_InputNumber_Thrice store the STR_InputNumber_Twice and additional STR_InputNumber
print(int(STR_InputNum)+int(STR_InputNum_Twice)+int(STR_InputNum_Thrice)) #All string is converted to Integer to add all 3, if integer conversion is not added, it will concat all the string
# Number 9 END